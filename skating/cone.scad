// Parametric cone for skating and sports

// User-defined parameters

bottom_diameter = 80; // [40:1:100]
shape_factor = 0.5; // [0:0.1:1]
height = 100; // [40:1:200]
thickness = 3; // [2:1:10]
hole = true; // [true, false]
hole_diameter = 10; // [1:1:20]

// Calculated parameters

top_diameter = bottom_diameter * shape_factor;

difference() {
    // outer cylinder
    cylinder(
        h  = height,
        r1 = bottom_diameter/2,
        r2 = top_diameter/2
    );
    // inner cylinder
    translate([0,0,-0.1]) cylinder(
        h  = height - thickness,
        r1 = bottom_diameter/2 - thickness,
        r2 = top_diameter/2 - thickness
    );
}
