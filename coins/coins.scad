// Parametric Euro coin designer
// Jorge Juan-Chico <jjchico@gmail.com>
// 2020-01-17

// Produces Euro-sized cylinders (coins) to use in supermarket trolleys or
// whereever you like. Optional convenient hole at the side in case you want
// to attach a piece of string, chain, etc.

//// User-selectable parameters ////////////////////////////////////////////

// Type of coin
coin = "1 euro"; // [2 euro, 1 euro, 50 cents, 20 cents, 10 cents, 5 cents, 2 cents, 1 cent]

// Curve smoothing factor
smooth = 5; // [0:1:9]

// Do you want a hole at the side?
side_hole = "yes"; // [yes, no]

// Hole diameter (% of coin diameter)
hole_width_percent = 12; // [5:1:40]

// Hole distance to the border (% of hole diameter)
hole_distance_percent = 50; // [10:5:200]

//// Caculated parameters //////////////////////////////////////////////////

// Coin size [diameter, thickness] in mm
size =
    coin == "1 cent"   ? [16.25, 1.67] :
    coin == "2 cents"  ? [18.75, 1.67] :
    coin == "5 cents"  ? [21.25, 1.67] :
    coin == "10 cents" ? [19.75, 1.93] :
    coin == "20 cents" ? [22.25, 2.14] :
    coin == "50 cents" ? [24.25, 2.38] :
    coin == "1 euro"   ? [23.25, 2.33] :
                         [25.75, 2.2];

radius = size[0]/2;
height = size[1];

hole_width = hole_width_percent * size[0] / 100;
hole_distance = hole_width * hole_distance_percent / 100;

$fa = 10 - smooth;
$fs = 0.1*1;

assert(hole_width + 2*hole_distance <= radius,
           "Hole too big");

module border() {
    translate([0,0,height/4])
        cylinder(h = 1.1*height/4, r1 = hole_width/2, r2 = 1.25*hole_width/2);
}

difference() {
    cylinder(h = height, r = radius, center = true);
    if(side_hole == "yes")
        translate([radius - hole_width/2 - hole_distance, 0, 0])
            union() {
                cylinder(h = 1.1*height, r = hole_width/2, center = true);
                border();
                mirror([0,0,1]) border();
            }
}