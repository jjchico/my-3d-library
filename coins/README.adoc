= Coins

Parametric coin generator. Currently support only Euro coins, but can be
easily extended. Very useful for supermarket shopping carts.

Optional side hole can be generated in cas you want to attach something to
the coin.

=== Printing

* Material: Any.
* Fill: 50% (PLA), >50% (TPU)
* Support: no
