// Simple parameterized washer

// Jorge Juan-Chico <jjchico@gmail.com>
// 2020-02-11

// Contour smothness
$fn = 80;   // [3:100]

// Washer heigth
height = 0.8;
// Outside diameter
outside = 10.4;
// Inside diameter
inside = 2.2;
// Cone ratio (0=no cone, 1=45 deg.)
ratio = 1;  // [0:0.1:2]

// Inside diameter, top face
inside2 = inside + ratio*height;

difference() {
    cylinder(h = height, r=outside/2);
    cylinder(h=height, r1=inside/2, r2=inside2/2);
}