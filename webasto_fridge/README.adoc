= Caravan fridge parts

== Webasto door retainer

Door retainer for Webasto Cruise Marine refrigerators.

* Item code: SGC00125AA
* Item description: Plastic retainer with shelter CR130C

https://www.indelwebastomarine.com/us/service/downloads/[Webasto parts web page].

image::retainer.jpg[Retainer]

=== Printing:

* Fill: 100%
* Support: yes, zig-zag, 15%
