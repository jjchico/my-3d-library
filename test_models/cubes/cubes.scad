// Test cubes for stringing and seam

size = 10;
base_height = size/10;

cube(size);

translate([2*size, 0, 0]) cube(size);

translate([5*size, 0, 0]) cube(size);

cube([6*size, size, base_height]);