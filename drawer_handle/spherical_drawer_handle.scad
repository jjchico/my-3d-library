// Drawer handle

$fn = 100;

diameter = 25;
cut = 3;
hole_diam = 3;

difference() {
    translate([0, 0, diameter/2 - cut])
        sphere(d = diameter);
    translate([0, 0, -cut/2])
        cube([diameter, diameter, cut], center = true);
    translate([0, 0, -cut/2])
        cylinder(h = diameter/2, r = hole_diam/2, $fn = 6);
}