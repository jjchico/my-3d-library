// Flexible support

base_height = 8;
side_height = 6;
inside_diameter_bottom = 20;
inside_diameter_top = 25;
inside_height = 12;
wall_thickness = 5;

height = base_height + side_height;


difference() {
    cylinder(
        h = height,
        r = inside_diameter_top/2 + wall_thickness
    );
    translate([0, 0, base_height+0.1])
        cylinder(
            h = inside_height,
            r1 = inside_diameter_bottom/2 + 0.1,
            r2 = inside_diameter_top/2 + 0.1
        );
}